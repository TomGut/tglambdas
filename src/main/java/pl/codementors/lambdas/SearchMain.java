package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Example for searching with lambdas.
 *
 * @author psysiu
 */
public class SearchMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8);

        System.out.println(list.stream().findFirst().orElse(-1));

        //Anonymous predicate.
        System.out.println(list.stream().filter(new Predicate<Integer>() {
            @Override
            public boolean test(Integer value) {
                return value > 9;
            }
        }).findFirst().orElse(-1));//Find returns Optional object so something else can be returned if no results.

        //Lambda predicate.
        System.out.println(list.stream().filter(value -> value > 2).findFirst().orElse(-1));
    }

}
