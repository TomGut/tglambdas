package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Example of using for each loop as lambda.
 *
 * @author psysiu
 */
public class ForEachMain {

    public static void main(String[] args) {
        //Quick way to make list with values.
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8) ;
        //Collections can not store simple types. Autoboxing is used by default.

        //Creating anonymous consumer.
        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer value) {
                System.out.println(value);
            }
        });

        System.out.println();

        //Using lambda.
        list.forEach(value -> {
            System.out.println(value);
        });

        System.out.println();

        //Using simple lambda.
        list.forEach(value -> System.out.println(value));
    }

}
