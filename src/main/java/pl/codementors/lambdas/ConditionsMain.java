package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Examples for conditions with lambdas.
 *
 * @author psysiu
 */
public class ConditionsMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8) ;

        boolean res = list.stream().allMatch(new Predicate<Integer>() {
            @Override
            public boolean test(Integer value) {
                return value < 10;
            }
        });
        System.out.println(res);

        res = list.stream().allMatch(value -> {
            return value < 10;
        });
        System.out.println(res);

        res = list.stream().allMatch(value -> value < 10);
        System.out.println(res);
    }

}
