package pl.codementors.lambdas;

import pl.codementors.lambdas.model.Formatter;
import pl.codementors.lambdas.model.NamedFormatter;

/**
 * Simple lambda example.
 *
 * @author psysiu
 */
public class FormatterMain {

    public static void main(String[] args) {

        String hello = "Hello World";

        //Instance of named class can be passed to method invocation.
        printFormatted(hello, new NamedFormatter());

        //Anonymous class can be created and assigned to variable.
        Formatter formatter = new Formatter() {
            @Override
            public String format(String text) {
                return text + " formatted by anonymous formatter";
            }
        };

        //Instance of anonymous class can be passed as variable.
        printFormatted(hello, formatter);

        //Instance of anonymous class can be created in method invocation.
        printFormatted(hello, new Formatter() {
            @Override
            public String format(String text) {
                return text + " formatted by another anonymous formatter";
            }
        });

        //Creating lambda. When passing only one param () can be omitted.
        printFormatted(hello, text -> {
            return text + " formatted by lambda";
        });

        //For one line methods {} and return can be omitted.
        printFormatted(hello, text -> text + " formatted by simple lambda");

    }

    /**
     * Prints provided text formatted in the specified way.
     *
     * @param text Text to be printed.
     * @param formatter Formatter to be used to format provided text.
     */
    public static void printFormatted(String text, Formatter formatter) {
        System.out.println(formatter.format(text));
    }

}
