package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

/**
 * Some collection manipulation tasks.
 *
 * @author psysiu
 * push mad from commit "Some tasks to be done"
 */
public class Tasks {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("wolf", "tiger", "turtle", "tortoise", "puma", "wolverine", "");

        System.out.println("task1:");
        task1(list);
        System.out.println("\ntask2:");
        task2(list);
        System.out.println("\ntask3:");
        task3(list);
        System.out.println("\ntask4:");
        task4(list);
        System.out.println("\ntask5:");
        task5(list);
        System.out.println("\ntask6:");
        task6(list);
        System.out.println("\ntask7:");
        task7(list);
        System.out.println("\ntask8:");
        task8(list);
        System.out.println("\ntask9:");
        task9(list);
        System.out.println("\ntask10:");
        task10(list);
        System.out.println("\ntask11:");
        task11(list);
        System.out.println("\ntask12:");
        task12(list);

    }

    public static void task1(List<String> list) {
        //print all elements
        /*

        //tu wyrażenie zapisane za pomocą instancji klasy abstrakcyjnej - mozliwośc lambdy pojawia się w żarówie
        //po kliknięciu wyszarzonej częśći tj. new Consumer
        list.stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
        */

        //wyrażenie zapisane lambdą
        //list.stream().forEach(s -> System.out.println(s));

        //skróceone dodatkowo
        //list.forEach(System.out::println);
    }

    public static void task2(List<String> list) {
        //print all elements in upper case

        list.stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s.toUpperCase());
            }
        });

        list.stream().map(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.toUpperCase();
            }
        });
    }

    public static void task3(List<String> list) {
        //print only elements with 4 letters

        list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                if(s.length() == 4){
                    return true;
                }
                return false;

                //można też
                //return s.length() == 4;
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });


        //lub
        //list.stream().filter(s -> s.length() == 4).forEach(System.out::println);

        //list.stream().filter(s -> s.length() ==4).forEach(System.out::println);

    }

//TODO spróbować zrobić to ifami

    public static void task4(List<String> list) {
        //print all elements sorted by number of letters

        list.stream().sorted(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
    }

    public static void task5(List<String> list) {
        //print all elements starting with 't'

        list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                if(s.startsWith("t")){
                    return true;
                }
                return false;
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
    }

    public static void task6(List<String> list) {
        //print NUMBER of all elements starting with w

        //odrazu drukujemy w sout cala lambdę
        System.out.println(list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                if(s.startsWith("w")){
                    return true;
                }
                return false;
            }
        }).count());
    }

    public static void task7(List<String> list) {
        //print "true" if any of the elements is empty, "false" otherwise

        /*

        moje
        list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                if(s.isEmpty()){
                    return true;
                }
                System.out.println(s + " false");
                return false;
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s + " true");
            }
        });
        */
        System.out.println(list.stream().anyMatch(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        }));

    }

    public static void task8(List<String> list) {
        //print "true" if all elements are empty, "false otherwise

        System.out.println(list.stream().allMatch(String::isEmpty));

    }

    public static void task9(List<String> list) {
        //print number of elements with more than 4 letters

        long count = list.stream().filter(s -> s.length() > 4).count();
        System.out.println(count);
    }

    public static void task10(List<String> list) {
        //print highest number of letters

        //to jest do najdłuższego stringa
        //list.stream().max((s1, s2) -> s1.length() - s2.length()).get().length();

        //można też zrobić tak
        System.out.println(list.stream().mapToInt(new ToIntFunction<String>() {
            @Override
            public int applyAsInt(String s) {
                return s.length();
            }
        }).max().orElse(0));

        //lub prościej
        //System.out.println(list.stream().mapToInt(String::length).max().getAsInt());

    }

    public static void task11(List<String> list) {
        //print lowest number of letters
        System.out.println(list.stream().mapToInt(String::length).min().getAsInt());
    }

    public static void task12(List<String> list) {
        //print sum of all numbers of letters

        System.out.println(list.stream().mapToInt(String::length).sum());
    }
}
